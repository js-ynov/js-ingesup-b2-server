const express = require('express');
const compression = require('compression');
const cors = require('cors');

const errorMiddleware = require('./common/error/error.middleware');
const { expressLogger } = require('./common/logger');
const appRouter = require('./app.router');

require('./common/mongo.config');

const app = express();

app.use(expressLogger);
app.use(express.json());
app.use(cors({
  exposedHeaders: 'x-total-count'
}));
app.use(compression());
app.use(appRouter);
app.use(errorMiddleware);

module.exports = app;
