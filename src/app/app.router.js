const express = require('express');
const { AppError, AppErrorTypes } = require('./common/error/error');
const matchRouter = require('./match/match.router');

const router = express.Router();

router
  .use('/matches', matchRouter)
  .use('*', (req, res, next) => {
    const error = new AppError(AppErrorTypes.ENDPOINT_NOT_FOUND, `No endpoint ${req.baseUrl} has been found`);
    next(error);
  });

module.exports = router;
