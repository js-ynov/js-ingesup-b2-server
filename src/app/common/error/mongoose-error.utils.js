const {CastError} = require('mongoose');
const {AppError, AppErrorTypes} = require('./error');

const invalidId = id => error => {
  if (error instanceof CastError) {
    return Promise.reject(new AppError(AppErrorTypes.DTO_INVALID_ID, `DTO id=${id} format is invalid`));
  }
};

const notFound = id => error => {
  if (error.name === 'VersionError') {
    return Promise.reject(new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No match with id=${id} has been found`));
  }
  return Promise.reject()
};

module.exports = {
  invalidId,
  notFound
};
