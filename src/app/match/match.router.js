const express = require('express');
const controller = require('./match.controller');

const router = express.Router();

router
  .get('/', controller.findAll)
  .get('/:id', controller.find)
  .post('/', controller.create)
  .put('/:id', controller.update)
  .delete('/:id', controller.remove);

module.exports = router;
