const Joi = require('@hapi/joi');

const schema = Joi.object({
  id: Joi.string(),
  homeTeam: Joi.string().required(),
  visitorTeam: Joi.string().required(),
  score: Joi.array().items(Joi.number()).length(2),
  date: Joi.string().isoDate().required()
});

module.exports = schema;
